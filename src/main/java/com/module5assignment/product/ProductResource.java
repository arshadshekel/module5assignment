package com.module5assignment.product;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.module5assignment.domain.Product;
import com.module5assignment.service.ProductService;

@RestController
//@Controller
public class ProductResource {

	@Autowired
	private ProductService productService;

	// HTTP Method : GET
	// URI : /users/vinodh/products/1
	@GetMapping(path = "/products/{id}")
	public Product getProduct(@PathVariable long id) {
		
		Product theProduct = productService.findById(id);
		return theProduct;
	}

	// HTTP Method : GET
	// URI: /users/vinodh/products
	@GetMapping(path = "/products")
	public List<Product> getAllProducts() {
		return productService.findAll();
	}

	// HTTP Method : DELETE
	// URI : /users/{username}/products/{id}
	@DeleteMapping(path = "/products/{id}")
	public ResponseEntity<Product> deleteProduct(@PathVariable long id) {
		productService.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	// HTTP Method : PUT
	// URI : /users/{username}/products/{id}
	@PutMapping(path = "/products/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateProduct(@PathVariable long id,
			@RequestBody Product theProduct) {
		Product loadedProduct = productService.findById(id);
		loadedProduct.setProductid(id);
		loadedProduct.setProductname(theProduct.getProductname());
		loadedProduct.setProductdescription(theProduct.getProductdescription());
        loadedProduct.setCategory(theProduct.getCategory());
        loadedProduct.setImage(theProduct.getImage());
        loadedProduct.setStock(theProduct.getStock());
		productService.save(loadedProduct);
	}

	// HTTP Method : POST
	// URI : /users/vinodh/products
	// Request Body ---- > JSON Data { }
	@PostMapping(path = "/products")
	public ResponseEntity<Product> createProduct(@Valid @RequestBody Product theProduct) {
		Product savedProduct = productService.save(theProduct);

		// Response Header : location
		// Status Code : 201
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedProduct.getProductid())
				.toUri();
		return ResponseEntity.created(location).build();
	}

}
