package com.module5assignment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.module5assignment.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByProductname(String name);
	

}