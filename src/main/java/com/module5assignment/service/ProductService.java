package com.module5assignment.service;

import java.util.List;

import com.module5assignment.domain.Product;

public interface ProductService {
	public List<Product> findAll();
	public Product save(Product theProduct);
	public Product deleteById(long theId);
	public Product findById(long theId);
}
