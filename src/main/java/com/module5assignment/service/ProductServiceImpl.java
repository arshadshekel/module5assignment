package com.module5assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.module5assignment.domain.Product;
import com.module5assignment.repository.ProductRepository;

@Service(value = "productService")
public class ProductServiceImpl implements ProductService{
	
	//depends on 'ProductRepository' 
//	private ProductRepository productRepository = new ProductRepositoryImpl(); //old code
	
	@Autowired
	private ProductRepository productRepository; //property name
	
//	public void updateProduct(Product theProduct) {
//	
//	}
	
	
	//spring default constructor
	public ProductServiceImpl() {
		
	}
	
	public ProductServiceImpl(ProductRepository productRepository) {
		super();
		System.out.println("spring container called constructor to  assemble 'productRepository' bean");
		this.productRepository = productRepository;
	}

	public void setProductRepository(ProductRepository productRepository) {
		System.out.println("spring container called setter method to  assemble 'productRepository' bean");
		this.productRepository = productRepository;
	}

	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	public Product save(Product theProduct) {
		return productRepository.save(theProduct);
	}

	@Override
	public Product deleteById(long theId) {
		 productRepository.deleteById(theId);
		 return null;
	}

	@Override
	public Product findById(long theId) {
		return productRepository.findById(theId).get();
	}

}
